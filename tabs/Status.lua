-- Color-coded indicator to show status of the project

Status = class()

Status.green = color(86, 185, 45, 255)
Status.blue = color(77, 103, 179, 255)
Status.yellow = color(192, 200, 41, 255)
Status.orange = color(212, 154, 33, 255)
Status.purple = color(147, 46, 155, 255)
Status.grey = color(104, 104, 104, 255)
Status.lightgrey = color(196, 196, 196, 255)
Status.darkgrey = color(76, 76, 76, 255)
Status.white = color(255, 255, 255, 255)

local N = 12


function Status:init(x, y, d, project)
    -- you can accept and set parameters here
    self.x = x
    self.y = y
    self.d = d
    self.project = project
    self.c = Status.grey
    self.arrow = 0
end

function Status:draw()
    -- Codea does not automatically call this method
    pushStyle()
    if self.working then
        local x,y,r = self.x,self.y,self.d/2
        local n = 6
        local w = 0.6
        local j = self.arrow - n
        strokeWidth(4.0)
        smooth()
        for i=1,N do
            local alpha = math.pi*2/N*i
            local sin,cos = math.sin(alpha),math.cos(alpha)
            if (j<i and i<=self.arrow) or i>N+j then
                -- lighter
                local k = self.arrow - i
                k = (k >= 0) and k or N+k
                stroke(Status.darkgrey:mix(Status.lightgrey, k/n))
            else
                stroke(Status.darkgrey)
            end
            line(x+sin*r*w, y+cos*r*w, x+sin*r, y+cos*r)
        end
    else
        strokeWidth(1.0)
        stroke(19, 19, 19, 255)
        fill(self.c)
        ellipse(self.x, self.y, self.d)
    end
    popStyle()
end

function Status:startAnimation()
    if self.working then
        return
    end
    self.arrow = 0
    self.working = true
    self._tween = tween(1.5, self, {arrow = N}, {
        easing = tween.easing.linear,
        loop = tween.loop.forever}, function()
            self.arrow = 0
        end)
end

function Status:stopAnimation()
    if self._tween then tween.stop(self._tween) end
    self.working = false
    
    local d = self.d
    self.d = d/4
    tween(0.5, self, {d = d}, tween.easing.cubicOut)
end
