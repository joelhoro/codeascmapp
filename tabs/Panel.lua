Panel = class()

function Panel:init(x, y, w, h)
    -- you can accept and set parameters here
    self.x = x
    self.y = y
    self.w = w
    self.h = h
    self.ypos = 0
    self.yposMax = 0
    self.children = {}
    
    local th, bh = 130, 150
    local bv = {vec2(x,y), vec2(x,y+bh), vec2(x+w,y+bh), vec2(x+w,y)}
    local tv = {vec2(x,y+h-th), vec2(x,y+h), vec2(x+w,y+h), vec2(x+w,y+h-th)}
    local c1 = color(0, 0, 0, 90)
    local c2 = color(0, 0, 0, 0)
    self.bottomShade = mesh()
    self.bottomShade.vertices = {bv[1],bv[2],bv[3],bv[4],bv[1],bv[3]}
    self.bottomShade.colors = {c1,c2,c2,c1,c1,c2}
    self.topShade = mesh()
    self.topShade.vertices = {tv[1],tv[2],tv[3],tv[4],tv[1],tv[3]}
    self.topShade.colors = {c2,c1,c1,c2,c2,c1}
end

function Panel:add(child, h)
    self.children[child] = true
    self.yposMax = math.max(self.yposMax, self.y - (child.y - 20))
end

function Panel:remove(child)
    self.children[child] = nil
end

function Panel:draw()
    -- Codea does not automatically call this method
    pushMatrix()
    clip(self.x, self.y, self.w, self.h)
    translate(0, self.ypos)
    for c,_ in pairs(self.children) do
        c:draw()
    end
    translate(0, -self.ypos)
    self.topShade:draw()
    self.bottomShade:draw()
    clip()
    popMatrix()
end

function Panel:inside(touch)
    local x,y,w,h = self.x,self.y,self.w,self.h
    if y <= touch.y and touch.y <= y+h then
        if x <= touch.x and touch.x <= x+w then
            return true
        end
    end
end

function cancelPanelTween()
    if panelTween then
        tween.stop(panelTween)
        panelTween = nil
    end
end

function Panel:relayTouch(touch)
    childTouchOk = true
    -- translate touch to account for scrolling
    local t = {x = touch.x, y = touch.y - self.ypos, state = touch.state}
    -- let children react to touch
    for c,_ in pairs(self.children) do
        if c.touched then
            c:touched(t)
        end
    end
end

function Panel:touched(touch)
    if self:inside(touch) then
        if touch.state == BEGAN then 
            -- delay the processing a bit to allow for scrolling
            childTouchOk = nil
            panelTween = tween.delay(0.1, function() self:relayTouch(touch) end)
        elseif touch.state == MOVING then
            -- cancel BEGAN touch event if we are scrolling the panel
            cancelPanelTween()
            self.ypos = math.min(math.max(0, self.ypos + touch.deltaY), self.yposMax)
            if childTouchOk then
                self:relayTouch(touch)
            end
        elseif touch.state == ENDED then
            if childTouchOk then
                self:relayTouch(touch)
            else
                cancelPanelTween() 
            end
        end
    end
end
